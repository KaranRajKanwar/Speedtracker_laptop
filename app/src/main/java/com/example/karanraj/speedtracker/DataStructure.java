package com.example.karanraj.speedtracker;
//TODO create a datastructure to hold the Sensor Information
public class DataStructure {
    private String personName;
    private String personEmail;
    private String timestamp;
    private String locationText;
    private String currentSpeed;

    public DataStructure(){

    }

    public DataStructure(String name,String email, String timestamp,String locationText,String currentSpeed){
        this.personName = name;
        this.personEmail = email;
        this.timestamp =timestamp;
        this.locationText = locationText;
        this.currentSpeed = currentSpeed;
    }

    public String getName() {
        return personName;
    }

    public void setName(String name) {
        this.personName = name;
    }

    public String getEmail() {
        return personEmail;
    }

    public void setEmail(String email) {
        this.personEmail = email;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getLocationText() {
        return locationText;
    }

    public void setLocationText(String locationText) {
        this.locationText = locationText;
    }

    public String getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(String currentSpeed) {
        this.currentSpeed = currentSpeed;
    }
}
