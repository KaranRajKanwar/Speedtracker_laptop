package com.example.karanraj.speedtracker;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RecyclerAdapter extends RecyclerView.Adapter {

    private String[] data;

    public RecyclerAdapter(String[] data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recycleritemlayout, parent , false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    String title = data[position];
    holder.itemView.setTag(title);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView RImage;
        TextView RText;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            RImage = (ImageView) itemView.findViewById(R.id.Rimage);
            RText = (TextView) itemView.findViewById(R.id.Rtext);
        }
    }
}